package com.castrolol.hidra.archergame.interfaces;

import com.castrolol.hidra.archergame.base.CollisionData;
import com.castrolol.hidra.archergame.objects.Zombie;
import com.castrolol.hidra.archergame.physics.Hitbox;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 10/01/2016.
 */
public interface ZombieBaseType {


    void reset();

    Hitbox[] getHitboxes();

    CollisionData getCollisionData(int collisionType);

    void paint(float delta, int xDraw, int yDraw, Graphics g);

    void update(float delta);

    int getStartX();

    int getStartY();

    int getWidth();

    int getHeight();

    boolean isAnimationCompleted();

    void setZombie(Zombie zombie);

    float getPointsFor(int reason);

    boolean isAttackingAnimationCompleted();

    boolean isPreparedToAttack();
}


