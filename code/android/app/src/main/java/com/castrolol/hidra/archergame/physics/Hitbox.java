package com.castrolol.hidra.archergame.physics;

import android.graphics.Rect;

import com.hidra.framework.interfaces.GameObject;

import java.util.Objects;

/**
 * Created by 'Luan on 05/01/2016.
 */
public class Hitbox {

    public final Rect rect;
    public final int type;
    public final Object source;

    public Hitbox(int type, Object source, Rect rect){
        this.rect = rect;
        this.type = type;
        this.source = source;
    }

    public boolean contains(int x, int y){
        return  rect.contains(x, y);
    }

}
