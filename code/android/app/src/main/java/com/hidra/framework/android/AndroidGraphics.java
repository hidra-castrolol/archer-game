package com.hidra.framework.android;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;

import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;
import com.hidra.framework.interfaces.SpriteFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class AndroidGraphics implements Graphics {

    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint;
    Rect srcRect;
    Rect dstRect;

    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();

        srcRect = new Rect();
        dstRect = new Rect();

    }


    @Override
    public Typeface loadFont(String name) {
        return Typeface.createFromAsset(assets, "fonts/" + name);
    }

    @Override
    public Image newImage(String fileName, ImageFormat format) {
        Bitmap.Config config = null;
        if (format == ImageFormat.RGB565) {
            config = Bitmap.Config.RGB_565;
        } else if (format == ImageFormat.ARGB4444) {
            config = Bitmap.Config.ARGB_4444;
        } else {
            config = Bitmap.Config.ARGB_8888;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = config;

        InputStream in = null;
        Bitmap bitmap = null;

        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Não foi possivel carregar o bitmap do asset '" + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Não foi possivel carregar o bitmap do asset '" + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }

        if (bitmap.getConfig() == Bitmap.Config.RGB_565) {
            format = ImageFormat.RGB565;
        } else if (bitmap.getConfig() == Bitmap.Config.ARGB_4444) {
            format = ImageFormat.ARGB4444;
        } else {
            format = ImageFormat.ARGB8888;
        }

        return new AndroidImage(bitmap, format);
    }

    @Override
    public SpriteFactory newSprite(String fileName, ImageFormat format, int frameWidth, int frameHeight, int frameCount) {
        return new AndroidSpriteFactory(newImage(fileName, format), frameWidth, frameHeight, frameCount);
    }

    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8, (color & 0xff));
    }

    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    @Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(x, y, x + width, y + height, paint);
    }

    @Override
    public void strokeRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(x, y, x + width, y + height, paint);
    }

    @Override
    public void drawArc(int x, int y, int size, int color) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

        canvas.drawCircle(x, y, size, paint);

    }

    @Override
    public void drawArc(int x, int y, int size, int startAngle, int endAngle, int color) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);


        canvas.drawArc(new RectF(x - (size / 2), y - (size / 2), x + (size / 2), y + (size / 2)), (float) (startAngle), (float) (endAngle), true, paint);
    }

    @Override
    public void drawImage(Image image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;

        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;

        canvas.drawBitmap(((AndroidImage) image).bitmap, srcRect, dstRect, null);

    }


    @Override
    public void strokeArc(int x, int y, int size, int startAngle, int endAngle, int color, float width) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(width);
        paint.setAntiAlias(true);


        canvas.drawArc(new RectF(x - (size / 2), y - (size / 2), x + (size / 2), y + (size / 2)), (float) (startAngle), (float) (endAngle), false, paint);
    }

    @Override
    public void drawImage(Image image, int x, int y) {
        canvas.drawBitmap(((AndroidImage) image).bitmap, x, y, null);
    }

    @Override
    public void drawImage(Image image, int x, int y, int rotatedeg) {
        canvas.save();
        canvas.rotate(360 - rotatedeg, x + (image.getWidth() / 2), y + (image.getHeight() / 2));
        canvas.drawBitmap(((AndroidImage) image).bitmap, x, y, null);
        canvas.restore();
    }

    @Override
    public void drawImage(Image image, int x, int y, int rotatedeg, Point center) {
        canvas.save();
        canvas.rotate(360 - rotatedeg, x + center.x, y + center.y);
        canvas.drawBitmap(((AndroidImage) image).bitmap, x, y, null);
        canvas.restore();

    }


    @Override
    public void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight) {
        drawScaledImage(image, x, y, width, height, srcX, srcY, srcWidht, srcHeight, 0);
    }

    @Override
    public void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight, int rotateAng) {

        drawScaledImage(image, x, y, width, height, srcX, srcY, srcWidht, srcHeight, rotateAng, new Point((image.getWidth() / 2), (image.getHeight() / 2)));

    }
    @Override
    public void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight, int rotateAng, Point center) {
        canvas.save();
        canvas.rotate((360 - rotateAng) % 360, x + center.x, y + center.y);

        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidht;
        srcRect.bottom = srcY + srcHeight;

        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + width;
        dstRect.bottom = y + height;


        canvas.drawBitmap(((AndroidImage) image).bitmap, srcRect, dstRect, null);

        canvas.restore();

    }

    @Override
    public void drawSprite(Sprite image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
        srcX = image.getSourceX() + Math.max(0, srcX);
        srcY = image.getSourceY() + Math.max(0, srcY);
        srcWidth = Math.max(image.getWidth(), srcWidth);
        srcHeight = Math.max(image.getHeight(), srcHeight);
        drawScaledImage(((AndroidSprite) image).source, x, y, image.getWidth(), image.getHeight(), srcX, srcY, srcWidth, srcHeight);

    }

    @Override
    public void drawSprite(Sprite image, int x, int y) {
        drawScaledImage(((AndroidSprite) image).source, x, y, image.getWidth(), image.getHeight(), image.getSourceX(), image.getSourceY(), image.getWidth(), image.getHeight());
    }

    @Override
    public void drawSprite(Sprite image, int x, int y, int rotatedeg) {
        drawScaledImage(((AndroidSprite) image).source, x, y, image.getWidth(), image.getHeight(), image.getSourceX(), image.getSourceY(), image.getWidth(), image.getHeight(), rotatedeg);
    }

    @Override
    public void drawScaledSprite(Sprite img, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight) {

        srcX = img.getSourceX() + Math.max(0, srcX);
        srcY = img.getSourceY() + Math.max(0, srcY);
        srcWidth = Math.max(img.getWidth(), srcWidth);
        srcHeight = Math.max(img.getHeight(), srcWidth);

        drawScaledImage(((AndroidSprite) img).source, x, y, img.getWidth(), img.getHeight(), srcX, srcY, srcWidth, srcHeight);

    }


    @Override
    public void drawString(String text, int x, int y, Paint paint) {


        canvas.drawText(text, x, y, paint);
    }

    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }

    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }

    @Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Paint.Style.FILL);
        canvas.drawARGB(a, r, g, b);
    }
}
