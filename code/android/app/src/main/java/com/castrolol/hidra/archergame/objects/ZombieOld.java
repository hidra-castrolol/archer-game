package com.castrolol.hidra.archergame.objects;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.castrolol.hidra.archergame.objects.zombie.customs.ZombieCustomFactory;
import com.castrolol.hidra.archergame.physics.CollisionHandler;
import com.castrolol.hidra.archergame.physics.Hitbox;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;

import java.util.Random;
import java.util.UUID;

/**
 * Created by 'Luan on 05/01/2016.

public class ZombieOld implements GameObject, CollisionHandler.HitboxProvider {

    public int getX() {
        return x;
    }

    public int getW() {
        return w;
    }

    public int getY() {
        return y;
    }


    public boolean isDying = false;
    private float deathTime = 0;
    private boolean dead;

    public boolean isDead() {
        return dead;
    }

    public ZombieBaseCustom getType() {
        return type;
    }

    public enum ZombieParts {
        Head, Chest, Pants
    }

    private int x = 0;
    private int y = 0;
    private int w = 75;
    private int h = 190;
    private int hp = 100;
    private final float speed;
    private float currSpeed;
    private final String id;
    private final Game game;
    private Image[] images;
    private Sprite zombieSprite;
    private Sprite hatSprite;
    private Sprite deathHeadSprite;
    private Sprite deathSprite;
    private Sprite hatDeathSprite;

    private Paint textPaint;
    private ZombieBaseCustom type;
    private int deathReason = ZombieParts.Chest.ordinal();
    private final Random random;

    public ZombieOld(Game game, float speed) {
        random = new Random();
        this.id = UUID.randomUUID().toString();
        this.game = game;
        this.speed = speed;
        textPaint = new Paint();
        textPaint.setColor(Color.RED);
        textPaint.setAntiAlias(true);


        zombieSprite = Assets.images.zombieWalkSpriteFactory.newSprite(25);
        hatSprite = Assets.images.zombieHatSpriteFactory.newSprite(25);
        hatDeathSprite = Assets.images.zombieDeathHatSpriteFactory.newSprite(10);
        hatDeathSprite.setRunOnce(true);
        deathHeadSprite = Assets.images.zombieHeadshotSpriteFactory.newSprite(8);
        deathHeadSprite.setRunOnce(true);
        deathSprite = Assets.images.zombieDeathSpriteFactory.newSprite(15);
        deathSprite.setRunOnce(true);


        init();
    }

    public ZombieOld init() {

        type = ZombieCustomFactory.getCurrent().create();
        type.setGame(game);
        type.setZombie(this);


        this.y = 565;
        this.x = 1300;
        zombieSprite.reset();
        hatSprite.reset();
        hatDeathSprite.reset();
        deathHeadSprite.reset();
        deathSprite.reset();
        this.currSpeed = type.getMaximumSpeed();
        this.dead = false;
        this.deathTime = 0;
        this.isDying = false;
        this.hp = type.getMaximumHp();

        CollisionHandler.getCurrent().listen(this.id, this);
        return this;
    }


    @Override
    public Hitbox[] getHitboxes() {

        return new Hitbox[]{
                new Hitbox(ZombieParts.Head.ordinal(), this, new Rect(x + 15, y + 10, x + w - 10, y + 50)),
                new Hitbox(ZombieParts.Chest.ordinal(), this, new Rect(x + 25, y + 50, x + w - 5, y + 120)),
                new Hitbox(ZombieParts.Pants.ordinal(), this, new Rect(x + 5, y + 120, x + w - 5, y + h)),
        };

    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();

        Sprite toDraw = zombieSprite;
        int xDraw = x - (int) ((float) w * .75f);
        int yDraw = y;

        if (isDying) {
            toDraw = deathHeadSprite;
            if (deathReason != ZombieParts.Head.ordinal()) {
                toDraw = deathSprite;
                xDraw -= 40;
            }
        }

        g.drawSprite(toDraw, xDraw, yDraw);
        if (!isDying) {
            type.drawAdornment(delta, x - (int) ((float) w * .75f), yDraw, g);
        } else {
            type.drawDeathAdornment(delta, x - (int) ((float) w * .75f), yDraw, g);

        }
    }

    @Override
    public void update(float delta) {

        if (isDying) {
            type.updateDeathAdornment(delta);
            if (deathReason == ZombieParts.Head.ordinal()) {
                deathHeadSprite.update(delta);
            } else {
                deathSprite.update(delta);
            }
            deathTime += delta;
            if (deathTime >= 300) {
                dead = true;
            }

            return;
        }

        zombieSprite.update(delta);
        type.updateAdornment(delta);
        if (x > 200) {
            x -= (int) (delta * currSpeed);
        } else {

        }

        if (currSpeed < speed) {
            currSpeed = Math.min(speed, currSpeed + (speed * delta / 100f));
        }

    }

    @Override
    public void dispose() {

    }

    @Override
    public void onCollided(int type) {
        int damage = 0;
        deathReason = type;
        float slow = 0;
        if (type == ZombieParts.Head.ordinal()) {
            damage = 100;
        } else if (type == ZombieParts.Chest.ordinal()) {
            damage = 70;
            slow = .5f;
        } else {
            damage = 15;
            slow = .95f;
        }

        applyDamage(damage, slow);

    }

    private void applyDamage(int damage, float slow) {

        hp -= damage;
        currSpeed -= (speed * slow);

        if (hp <= 0) {
            isDying = true;
            CollisionHandler.getCurrent().remove(this.id);
        }

    }
}
 */