package com.hidra.framework.android;

import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;
import com.hidra.framework.interfaces.SpriteFactory;

/**
 * Created by 'Luan on 05/01/2016.
 */
public class AndroidSpriteFactory implements SpriteFactory {



    private int frameWidth;
    private int frameHeight;
    private int frameCount;
    private Image image;

    public AndroidSpriteFactory(Image image, int frameWidth, int frameHeight, int frameCount) {
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameCount = frameCount;
        this.image = image;
    }

    @Override
    public Sprite newSprite(float timePerFrame) {

        return new AndroidSprite( image,  frameWidth,  frameHeight,  frameCount, timePerFrame);
    }
}
