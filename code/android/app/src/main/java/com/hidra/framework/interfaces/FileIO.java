package com.hidra.framework.interfaces;

import android.content.SharedPreferences;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface FileIO {

        InputStream readFile(String file) throws IOException;

        OutputStream writeFile(String file) throws IOException;

        InputStream readAsset(String file) throws IOException;

        public SharedPreferences getSharedPref();

}
