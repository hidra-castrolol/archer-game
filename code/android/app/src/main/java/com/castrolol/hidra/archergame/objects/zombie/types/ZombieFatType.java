package com.castrolol.hidra.archergame.objects.zombie.types;

import android.graphics.Color;
import android.graphics.Rect;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.base.CollisionData;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseType;
import com.castrolol.hidra.archergame.objects.Zombie;
import com.castrolol.hidra.archergame.physics.Hitbox;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 28/03/2016.
 */
public class ZombieFatType implements ZombieBaseType {
    public final int HEAD = 0;
    public final int CHEST = 1;
    public final int BELLY = 2;
    public final int PANTS = 3;

    private final CollisionData headCollisionData = new CollisionData(HEAD, 100, 0f);
    private final CollisionData chestCollisionData = new CollisionData(CHEST, 70, 0.5f);
    private final CollisionData pantsCollisionData = new CollisionData(PANTS, 15, 0.95f);

    private final Sprite walkSprite;
    private int deathReason;

    public ZombieFatType() {
        walkSprite = Assets.images.fatZombieWalkSpriteFactory.newSprite(18);

    }

    private Zombie zombie;

    public void setZombie(Zombie zombie) {
        this.zombie = zombie;
    }

    @Override
    public float getPointsFor(int reason) {
        switch (reason) {
            case HEAD:
                return 100;
            case CHEST:
                return 50;
            case PANTS:
                return 15;
        }
        return 0;
    }

    @Override
    public boolean isAttackingAnimationCompleted() {
        return true;
    }

    @Override
    public boolean isPreparedToAttack() {
        return true;
    }


    @Override
    public void reset() {
        walkSprite.reset();
    }

    @Override
    public Hitbox[] getHitboxes() {
        return new Hitbox[]{
                new Hitbox(HEAD, this, new Rect(zombie.x  - 55, zombie.y - 40 , zombie.x - 5, zombie.y )),
                new Hitbox(CHEST, this, new Rect(zombie.x  - 75, zombie.y , zombie.x + 25, zombie.y + 50)),
                new Hitbox(BELLY, this, new Rect(zombie.x - 100, zombie.y + 50, zombie.x + 25 , zombie.y + 120)),
                new Hitbox(PANTS, this, new Rect(zombie.x - 100, zombie.y + 120, zombie.x + 25, zombie.y + 185)),
        };
    }

    @Override
    public CollisionData getCollisionData(int collisionType) {
        deathReason = collisionType;

        switch (collisionType) {
            case HEAD:
                return headCollisionData;
            case CHEST:
                return chestCollisionData;
            case PANTS:
                return pantsCollisionData;
        }

        return CollisionData.empty;

    }

    @Override
    public void paint(float delta, int xDraw, int yDraw, Graphics g) {

        g.drawSprite(walkSprite, xDraw, yDraw - 50);

        Hitbox[] hitboxes = getHitboxes();


    }

    @Override
    public void update(float delta) {



        walkSprite.update(delta);

        int frameIndex = walkSprite.getFrameIndex();
        boolean podeCaminhar = frameIndex > 3 && frameIndex <= 7 ;

        if ( (podeCaminhar || zombie.x > 1200) && zombie.x > 380) {
            float speed = zombie.speed;

            if(zombie.x > 1200){
                speed *= 4;
            }

            zombie.x -= Math.round(speed * (delta * .5f));
        }

    }

    @Override
    public int getStartX() {
        return 1480;
    }

    @Override
    public int getStartY() {
        return 565;
    }

    @Override
    public int getWidth() {
        return 285;
    }

    @Override
    public int getHeight() {
        return 250;
    }

    @Override
    public boolean isAnimationCompleted() {

        return true;
    }
}
