package com.hidra.framework.interfaces;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public abstract class Screen {
    protected final Game game;

    protected boolean handleChildren = true;

    protected List<GameObject> gameObjects;

    public Screen(Game game){
        this.game = game;
        gameObjects = new ArrayList<>();
    }

    public void add(GameObject gameObject){
        gameObjects.add(gameObject);
    }

    public void updateAll(float delta) {
        update(delta);
        if(handleChildren)updateObjects(delta);
    }

    public void updateObjects(float delta){
        for(GameObject gameObject : gameObjects){
            gameObject.update(delta);
        }
    }

    public void paintAll(float delta){
        paint(delta);
        if(handleChildren)paintObjects(delta);
    }

    public void paintObjects(float delta){
        for(GameObject gameObject : gameObjects){
            gameObject.paint(delta);
        }
    }

    public abstract void update(float deltaTime);
    public abstract void paint(float deltaTime);
    public abstract void pause();
    public abstract void resume();
    public abstract void dispose();
    public abstract void backButton();

}
