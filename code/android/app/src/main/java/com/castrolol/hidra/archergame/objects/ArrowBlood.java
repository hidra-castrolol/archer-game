package com.castrolol.hidra.archergame.objects;

import android.graphics.Point;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.interfaces.FinishListener;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 06/01/2016.
 */
public class ArrowBlood implements GameObject {



    private final FinishListener finishListener;
    private final Game game;
    private int x;
    private int y;
    private final Sprite sprite;

    public ArrowBlood(Game game, FinishListener finishListener){
        this.finishListener = finishListener;
        this.game = game;
        sprite = Assets.images.bloodSpriteFactory.newSprite(5);
        sprite.setRunOnce(true);
    }

    public ArrowBlood init(Point point){
        this.x = point.x;
        this.y = point.y;
        this.sprite.reset();
        return this;
    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();
        g.drawSprite(sprite, x-80, y);


    }

    @Override
    public void update(float delta) {
        sprite.update(delta);
        if(sprite.isFinished()){
            finishListener.onFinished(this);
        }
    }

    @Override
    public void dispose() {
        sprite.dispose();
    }
}
