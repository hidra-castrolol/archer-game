package com.castrolol.hidra.archergame.objects.zombie.customs;

import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseType;
import com.castrolol.hidra.archergame.objects.zombie.types.ZombieFatType;
import com.hidra.framework.base.Pool;

import java.util.Random;

/**
 * Created by 'Luan on 09/01/2016.
 */
public class ZombieCustomFactory {

    private final Pool<ZombieNormalCustom> poolNormal;
    private final Pool<ZombieHatCustom> poolHat;
    private final Random random;

    public ZombieCustomFactory() {
        random = new Random();

        poolNormal = new Pool<>(new Pool.PoolObjectFactory<ZombieNormalCustom>() {
            @Override
            public ZombieNormalCustom createObject() {
                return new ZombieNormalCustom();
            }
        }, 30);


        poolHat = new Pool<>(new Pool.PoolObjectFactory<ZombieHatCustom>() {
            @Override
            public ZombieHatCustom createObject() {
                return new ZombieHatCustom();
            }
        }, 30);

    }

    private static ZombieCustomFactory current;

    public static ZombieCustomFactory getCurrent() {
        if (current == null) {
            current = new ZombieCustomFactory();
        }
        return current;
    }

    public void free(ZombieBaseCustom zombieType) {

        if (zombieType instanceof ZombieHatCustom) {
            poolHat.free((ZombieHatCustom) zombieType);
        } else if (zombieType instanceof ZombieNormalCustom) {
            poolNormal.free((ZombieNormalCustom) zombieType);
        }

    }

    public ZombieBaseCustom create(ZombieBaseType type) {
        ZombieBaseCustom custom = chooseType(type);
        custom.reset();
        return custom;
    }

    private ZombieBaseCustom chooseType(ZombieBaseType type) {

        int n = random.nextInt(100);

        if (type instanceof ZombieFatType) {
            return poolNormal.newObject();
        }

        if (n <= 30) {
            return poolHat.newObject();
        }

        return poolNormal.newObject();

    }


}
