package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Sound {

    void play(float volume);

    void dispose();

}
