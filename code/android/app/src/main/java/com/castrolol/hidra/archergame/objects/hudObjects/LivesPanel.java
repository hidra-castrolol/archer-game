package com.castrolol.hidra.archergame.objects.hudObjects;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.objects.Archer;
import com.hidra.framework.android.Event;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 25/01/2016.
 */
public class LivesPanel implements GameObject {

    private final Game game;
    private final Sprite heartSprite;

    private Event.EventListener<Archer> onArcherCreatedHandler;
    private Event.EventListener<Archer> onArcherDamagedHandler;

    private int lives = 0;
    private int maxLives = 0;
    private int displayLives = 0;

    public LivesPanel(Game game){
        this.game = game;
        heartSprite = Assets.images.hearthExplodeSpriteFactory.newSprite(10);
        heartSprite.setRunOnce(true);

        setUpEvents();

    }

    private void setUpEvents() {

        Archer.onCreated.listen(onArcherCreatedHandler = new Event.EventListener<Archer>() {
            @Override
            public void handle(String eventName, Archer source, Object... params) {
                LivesPanel.this.lives = source.getLives();
                LivesPanel.this.maxLives = source.getLives();
                LivesPanel.this.displayLives = source.getLives();
            }
        });

        Archer.onDamage.listen(onArcherDamagedHandler = new Event.EventListener<Archer>() {
            @Override
            public void handle(String eventName, Archer source, Object... params) {
                LivesPanel.this.lives = source.getLives();
                heartSprite.reset();
            }
        });

    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();
        Image heart;

        for(int i = 0; i < maxLives; i++){
            int x = (1200 - (i * 72));
            if(i < lives) {
                heart = Assets.images.hearthFull;
            }else{
                if(i + 1   == displayLives ){
                    g.drawSprite(heartSprite, x, 10);
                    continue;
                }else {
                    heart = Assets.images.hearthEmpty;
                }
            }

            g.drawImage(heart, x, 10);

        }

    }

    @Override
    public void update(float delta) {


        heartSprite.update(delta);

        if(lives != displayLives && heartSprite.isFinished()){
            displayLives--;
        }

    }

    @Override
    public void dispose() {
        heartSprite.dispose();
        Archer.onDamage.remove(onArcherDamagedHandler);
        Archer.onCreated.remove(onArcherCreatedHandler);
    }
}
