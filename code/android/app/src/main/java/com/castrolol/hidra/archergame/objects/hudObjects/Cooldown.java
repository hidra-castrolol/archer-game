package com.castrolol.hidra.archergame.objects.hudObjects;

import android.graphics.Color;

import com.castrolol.hidra.archergame.objects.Timer;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 25/01/2016.
 */
public class Cooldown implements GameObject {

    private final Game game;
    private int delay;
    private Timer timer;

    public Cooldown(Game game, int delay){
        this.game = game;
        this.delay = delay;
        timer =  Timer.create();
        timer.isElapsed(1);
    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();

        float elapsed = Math.min(1f, timer.getElapsed() / delay);

        int colorFill =  Color.argb(125, 125, 158, 205);
        int colorStroke =  Color.argb(255, 125, 158, 205);
        if(isElapsed()){
            colorFill =  Color.argb(125, 52, 209, 70);
            colorStroke =  Color.argb(255, 52, 209, 70);
        }


        g.drawArc(65, 480, 44, 0, 360, Color.DKGRAY);
        g.drawArc(65, 480, 40, 0, (int)(elapsed * 360), colorFill);
        g.strokeArc(65, 480, 40, 0, (int) (elapsed * 360),colorStroke, 4);

    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void dispose() {

    }

    public boolean isElapsed() {
        return timer.isElapsed(delay);
    }

    public void reset() {
        timer.reset();
    }
}
