package com.hidra.framework.android;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.hidra.framework.interfaces.Audio;
import com.hidra.framework.interfaces.Music;
import com.hidra.framework.interfaces.Sound;

import java.io.IOException;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class AndroidAudio implements Audio {

    AssetManager assets;
    SoundPool soundPool;

    public AndroidAudio(Activity activity){
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
    }

    @Override
    public Music createMusic(String file) {
        try {
            AssetFileDescriptor assetFileDescriptor = assets.openFd(file);
            return new AndroidMusic(assetFileDescriptor);
        }catch (IOException e){
            throw new RuntimeException("Não foi possivel carregar musica '" + file + "'");
        }
    }

    @Override
    public Sound createSound(String file) {
        try {
            AssetFileDescriptor assetFileDescriptor = assets.openFd(file);
            int soundId = soundPool.load(assetFileDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        }catch (IOException e){
            throw new RuntimeException("Não foi possivel carregar musica '" + file + "'");
        }
    }
}
