package com.castrolol.hidra.archergame.interfaces;

/**
 * Created by 'Luan on 06/01/2016.
 */
public interface FinishListener {
    void onFinished(Object source);
}
