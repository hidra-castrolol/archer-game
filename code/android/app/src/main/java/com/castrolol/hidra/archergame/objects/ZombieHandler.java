package com.castrolol.hidra.archergame.objects;

import android.graphics.Point;

import com.castrolol.hidra.archergame.interfaces.FinishListener;
import com.castrolol.hidra.archergame.objects.zombie.customs.ZombieCustomFactory;
import com.castrolol.hidra.archergame.objects.zombie.types.ZombieTypeFactory;
import com.castrolol.hidra.archergame.physics.CollisionHandler;
import com.hidra.framework.base.Pool;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Luan on 05/01/2016.
 */
public class ZombieHandler implements GameObject, FinishListener {



    private void onDeath(Zombie zombie) {
        zombies.remove(zombie);
        zombiePool.free(zombie);
    }


    Pool<Zombie> zombiePool;
    Pool<ArrowBlood> bloodPool;
    private final Game game;
    private List<ArrowBlood> bloods;
    private List<Arrow> arrows;
    private List<Zombie> zombies;
    public long lastSpawnedTime;

    public ZombieHandler(Game game, List<Arrow> arrows) {
        lastSpawnedTime = 0;
        zombies = new ArrayList<>();
        bloods = new ArrayList<>();
        this.arrows = arrows;
        this.game = game;

        zombiePool = new Pool<>(new Pool.PoolObjectFactory<Zombie>() {
            @Override
            public Zombie createObject() {
                return new Zombie(ZombieHandler.this.game);
            }
        }, 10);

        bloodPool = new Pool<>(new Pool.PoolObjectFactory<ArrowBlood>() {
            @Override
            public ArrowBlood createObject() {
                return new ArrowBlood(ZombieHandler.this.game, ZombieHandler.this);
            }
        }, 20);

    }

    @Override
    public void paint(float delta) {

        for (Zombie zombie : zombies) {
            zombie.paint(delta);
        }
        List<ArrowBlood> bloodToDraw = new ArrayList<>(bloods);
        for(ArrowBlood blood : bloodToDraw){

            blood.paint(delta);
        }
    }

    @Override
    public void update(float delta) {
        List<ArrowBlood> bloodToUpdate = new ArrayList<>(bloods);
        for(ArrowBlood blood : bloodToUpdate){
            blood.update(delta);
        }


        List<Arrow> arrowsToRemove = new ArrayList<>();
        List<Zombie> zombiesToRemove = new ArrayList<>();



        for(Zombie zombie : zombies) {
            zombie.update(delta);
            if(zombie.isDead()){
                zombiesToRemove.add(zombie);
                ZombieCustomFactory.getCurrent().free(zombie.custom);
                ZombieTypeFactory.getCurrent().free(zombie.type);
            }
        }

        for(Arrow arrow : arrows){
            Point collided  = CollisionHandler.getCurrent().checkCollisions(arrow.getPositions());
            if(collided != null){
                arrowsToRemove.add(arrow);
                bloods.add(bloodPool.newObject().init(collided));
            }
        }

        for (Arrow arrow : arrows) {
            if (arrow.getX() > 1300 || arrow.getY() > 900) {
                if (!arrowsToRemove.contains(arrow)) {
                    arrowsToRemove.add(arrow);
                    break;
                }
            }

        }

        for (Arrow arrow : arrowsToRemove) {
            arrows.remove(arrow);
        }

        for (Zombie zombie : zombiesToRemove) {
            onDeath(zombie);
        }

        if (zombies.size() < 1 && cooldownHasReached()) {
            zombies.add(zombiePool.newObject().init());
            lastSpawnedTime = System.nanoTime();
        }

    }

    private boolean cooldownHasReached() {
        return ((System.nanoTime() - lastSpawnedTime) / 10000000.000f) > 450;
    }

    @Override
    public void dispose() {

    }

    @Override
    public void onFinished(Object source) {
        if(source instanceof ArrowBlood){
            bloods.remove(source);
            bloodPool.free((ArrowBlood)source);
        }
    }
}
