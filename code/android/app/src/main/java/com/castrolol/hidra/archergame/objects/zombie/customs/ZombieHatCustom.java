package com.castrolol.hidra.archergame.objects.zombie.customs;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 09/01/2016.
 */
public class ZombieHatCustom extends ZombieBaseCustom {


    private Sprite hatSprite;
    private Sprite hatDeathSprite;
    private Sprite attackingHatSprite;


    public ZombieHatCustom() {

        hatSprite = Assets.images.zombieHatSpriteFactory.newSprite(25);
        hatDeathSprite = Assets.images.zombieDeathHatSpriteFactory.newSprite(10);
        hatDeathSprite.setRunOnce(true);
        attackingHatSprite = Assets.images.zombieAttackHatSpriteFactory.newSprite(15);
        attackingHatSprite.setRunOnce(true);


    }


    @Override
    public int getMaximumHp() {
        return 180;
    }

    @Override
    public float getMaximumSpeed() {
        return 0.5f;
    }

    @Override
    public float getRecoverySpeedAmount() {
        return 0;
    }

    @Override
    public void paint(float delta, int x, int y, Graphics g){
        if(zombie.isDying()){
            drawDeathAdornment(delta, x, y, g);
        }else{
            drawAdornment(delta, x, y, g);
        }
    }

    @Override
    public void update(float delta){
        if(zombie.isDying()){
            updateDeathAdornment(delta);
        }else{
            updateAdornment(delta);
        }
    }

    private void drawAdornment(float delta, int x, int y, Graphics g) {

        if(zombie.isAttacking()){
            g.drawSprite(attackingHatSprite, x, y);
            return;
        }
        g.drawSprite(hatSprite, x, y);
    }

    private void drawDeathAdornment(float delta, int x, int y, Graphics g) {
        g.drawSprite(hatDeathSprite, x, y);

    }

    private void updateAdornment(float delta) {
        if(zombie.isAttacking()){
            attackingHatSprite.update(delta);
            return;
        }
        hatSprite.update(delta);
    }

    private void updateDeathAdornment(float delta) {
        hatDeathSprite.update(delta);

    }

    @Override
    public void reset() {
        hatDeathSprite.reset();
        hatSprite.reset();
        attackingHatSprite.reset();
    }

    @Override
    public boolean isAnimationCompleted(){
        return hatDeathSprite.isFinished();
    }

    @Override
    public float getMultiplier() {
        return 1.2f;
    }

}
