package com.castrolol.hidra.archergame.objects.zombie.types;

import android.graphics.Rect;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.base.CollisionData;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseType;
import com.castrolol.hidra.archergame.objects.Zombie;
import com.castrolol.hidra.archergame.physics.Hitbox;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 10/01/2016.
 */
public class ZombieNormalType implements ZombieBaseType {
    public final int HEAD = 0;
    public final int CHEST = 1;
    public final int PANTS = 2;

    private final CollisionData headCollisionData = new CollisionData(HEAD, 100, 0f);
    private final CollisionData chestCollisionData = new CollisionData(CHEST, 70, 0.5f);
    private final CollisionData pantsCollisionData = new CollisionData(PANTS, 15, 0.95f);

    private final Sprite walkSprite;
    private final Sprite deathSprite;
    private final Sprite headshotSprite;
    private final Sprite attackingSprite;
    private int deathReason;

    public ZombieNormalType(){
        walkSprite = Assets.images.zombieWalkSpriteFactory.newSprite(25);
        deathSprite = Assets.images.zombieDeathSpriteFactory.newSprite(15);
        deathSprite.setRunOnce(true);
        headshotSprite = Assets.images.zombieHeadshotSpriteFactory.newSprite(8);
        headshotSprite.setRunOnce(true);
        attackingSprite = Assets.images.zombieAttackSpriteFactory.newSprite(15);
        attackingSprite.setRunOnce(true);

    }

    private Zombie zombie;

    public void setZombie(Zombie zombie){
        this.zombie = zombie;
    }

    @Override
    public float getPointsFor(int reason) {
        switch (reason){
            case HEAD:
                return 100;
            case CHEST:
                return 50;
            case PANTS:
                return 15;
        }
        return 0;
    }

    @Override
    public boolean isAttackingAnimationCompleted() {
        return attackingSprite.isFinished();
    }

    @Override
    public boolean isPreparedToAttack() {
        return attackingSprite.getFrameIndex() >= 4;
    }


    @Override
    public void reset() {
        walkSprite.reset();
        deathSprite.reset();
        headshotSprite.reset();
        attackingSprite.reset();
    }

    @Override
    public Hitbox[] getHitboxes() {
        return new Hitbox[]{
                new Hitbox(HEAD, this, new Rect(zombie.x + 15, zombie.y + 10, zombie.x + zombie.w - 10, zombie.y + 50)),
                new Hitbox(CHEST, this, new Rect(zombie.x + 25, zombie.y + 50, zombie.x + zombie.w - 5, zombie.y + 120)),
                new Hitbox(PANTS, this, new Rect(zombie.x + 5, zombie.y + 120, zombie.x + zombie.w - 5, zombie.y + zombie.h)),
        };
    }

    @Override
    public CollisionData getCollisionData(int collisionType) {
        deathReason = collisionType;

        switch (collisionType){
            case HEAD:
                return headCollisionData;
            case CHEST:
                return chestCollisionData;
            case PANTS:
                return pantsCollisionData;
        }

        return CollisionData.empty;

    }

    @Override
    public void paint(float delta, int xDraw, int yDraw, Graphics g) {
        if(zombie.isDying()) {
            if(deathReason == HEAD){
                g.drawSprite(headshotSprite, xDraw, yDraw);
            }else{
                g.drawSprite(deathSprite, xDraw - 40, yDraw);
            }
        }else if(zombie.isAttacking()){
            g.drawSprite(attackingSprite, xDraw, yDraw);
        }else{
            g.drawSprite(walkSprite, xDraw, yDraw);
        }
    }

    @Override
    public void update(float delta) {

        if (zombie.x > 200) {
            zombie.x -= Math.round(zombie.speed * (delta * .5f));
        }

        if(zombie.isDying()) {
            if(deathReason == HEAD){
                headshotSprite.update(delta);
            }else{
                deathSprite.update(delta);
            }
        }else if(zombie.isAttacking()){
            attackingSprite.update(delta);
        }else{
            walkSprite.update(delta);
        }
    }

    @Override
    public int getStartX() {
        return 1300;
    }

    @Override
    public int getStartY() {
        return 565;
    }

    @Override
    public int getWidth() {
        return 75;
    }

    @Override
    public int getHeight() {
        return 190;
    }

    @Override
    public boolean isAnimationCompleted() {

        if(deathReason == HEAD){
            return headshotSprite.isFinished();
        }else{
            return deathSprite.isFinished();
        }

    }
}
