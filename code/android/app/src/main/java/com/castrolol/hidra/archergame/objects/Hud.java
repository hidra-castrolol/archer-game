package com.castrolol.hidra.archergame.objects;

import com.castrolol.hidra.archergame.objects.hudObjects.LivesPanel;
import com.castrolol.hidra.archergame.objects.hudObjects.PointsPanel;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Luan on 15/01/2016.
 */
public class Hud implements GameObject {

    public final Game game;
    public List<GameObject> objects = new ArrayList<>();

    public Hud(Game game){
        this.game = game;
        setUpElements();
    }

    private void setUpElements() {
        objects.add(new LivesPanel(game));
        objects.add(new PointsPanel(game));
    }


    @Override
    public void paint(float delta) {
        for(GameObject object : objects) object.paint(delta);
    }

    @Override
    public void update(float delta) {
        for(GameObject object : objects) object.update(delta);

    }

    @Override
    public void dispose() {
        for(GameObject object : objects) object.dispose();

    }
}
