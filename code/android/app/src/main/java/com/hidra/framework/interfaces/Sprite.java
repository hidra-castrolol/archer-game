package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 05/01/2016.
 */
public abstract class Sprite {

    protected boolean runOnce = false;
    protected int frameIndex;
    protected int frameCount;
    protected float timePerFrame;
    protected float timeElapsed;
    protected boolean finished = false;


    public void update(float delta){

        timeElapsed += delta;
        if(getTimeElapsed() >= getTimePerFrame()){
            if(runOnce && frameIndex == frameCount - 1){
                finished = true;
                return;
            }
            frameIndex += 1;

            frameIndex %= frameCount;
            timeElapsed = 0;
        }

    }




    public int getFrameIndex() {
        return frameIndex;
    }

    public void setFrameIndex(int frameIndex) {
        this.frameIndex = frameIndex;
    }

    public float getTimePerFrame() {
        return timePerFrame;
    }

    public void setTimePerFrame(float timePerFrame) {
        this.timePerFrame = timePerFrame;
    }

    public float getTimeElapsed() {
        return timeElapsed;
    }

    public void reset(){
        this.frameIndex = 0;
        this.timeElapsed = 0;
        finished = false;
    }

    public void setRunOnce(boolean runOnce){
        this.runOnce = runOnce;
    }

    public abstract int getHeight();

    public abstract int getWidth();

    public abstract int getSourceX();

    public abstract int getSourceY();

    public void dispose() {

    }

    public boolean isFinished() {
        return finished;
    }
}
