package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 05/01/2016.
 */
public interface SpriteFactory {

    Sprite newSprite(float timePerFrame);

}
