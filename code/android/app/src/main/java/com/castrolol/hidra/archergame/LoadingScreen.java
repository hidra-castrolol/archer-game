package com.castrolol.hidra.archergame;

import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.Paint;

import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Screen;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class LoadingScreen extends Screen {
    Paint paint;
    public LoadingScreen(Game game) {
        super(game);

        Assets.fonts.drifType = game.getGraphics().loadFont("Drifttype.ttf");
        paint = new Paint();
        paint.setTextSize(30);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setAntiAlias(true);
        paint.setTypeface(Assets.fonts.drifType);
        paint.setColor(Color.RED);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();


        Assets.fonts.nightBird = game.getGraphics().loadFont("Nightbird.ttf");

        Assets.images.gameBackground = g.newImage("background-game.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.menuBackground = g.newImage("background-menu.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.playButton = g.newImage("play-button.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.playButtonDown = g.newImage("play-button-down.png", Graphics.ImageFormat.ARGB8888);

        Assets.images.shootIndicatorArrow = g.newImage("indicator-arrow.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.shootIndicatorBoard = g.newImage("indicator-board.png", Graphics.ImageFormat.ARGB8888);

        Assets.images.archer0 = g.newImage("archer-0-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer7 = g.newImage("archer-7-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer15 = g.newImage("archer-15-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer22 = g.newImage("archer-22-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer30 = g.newImage("archer-30-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer37 = g.newImage("archer-37-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer45 = g.newImage("archer-45-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer52 = g.newImage("archer-52-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer60 = g.newImage("archer-60-graus.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.archer67 = g.newImage("archer-67-graus.png", Graphics.ImageFormat.ARGB8888);

        Assets.images.bow1 = g.newImage("bow-1.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow2 = g.newImage("bow-2.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow3 = g.newImage("bow-3.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow4 = g.newImage("bow-4.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow5 = g.newImage("bow-5.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow6 = g.newImage("bow-6.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow7 = g.newImage("bow-7.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow8 = g.newImage("bow-8.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow9 = g.newImage("bow-9.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.bow10 = g.newImage("bow-10.png", Graphics.ImageFormat.ARGB8888);


        Assets.images.arrow = g.newImage("arrow.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.arrowBig = g.newImage("big-arrow.png", Graphics.ImageFormat.ARGB8888);


        Assets.images.bloodSpriteFactory = g.newSprite("blood-sprite.png", Graphics.ImageFormat.ARGB8888, 63, 40, 7);

        Assets.images.blood = g.newImage("blood.png", Graphics.ImageFormat.ARGB8888);

        Assets.images.zombieWalkSpriteFactory = g.newSprite("zombie-sprite.png", Graphics.ImageFormat.ARGB8888, 157, 190, 12);
        Assets.images.zombieHatSpriteFactory = g.newSprite("zombie-hat-sprite.png", Graphics.ImageFormat.ARGB8888, 157, 190, 12);


        Assets.images.fatZombieWalkSpriteFactory = g.newSprite("fat-zombie-walk-sprite.png", Graphics.ImageFormat.ARGB8888, 285, 250, 13);

        Assets.images.zombieHeadshotSpriteFactory = g.newSprite("zombie-dying-sprite.png", Graphics.ImageFormat.ARGB8888,383, 190, 7 );
        Assets.images.zombieDeathSpriteFactory = g.newSprite("zombie-death-alt-sprite.png", Graphics.ImageFormat.ARGB8888,197, 190, 8 );
        Assets.images.zombieDeathHatSpriteFactory = g.newSprite("zombie-hat-death-sprite.png", Graphics.ImageFormat.ARGB8888, 157, 190, 12);
        Assets.images.zombieAttackSpriteFactory = g.newSprite("zombie-attacking.png", Graphics.ImageFormat.ARGB8888, 157, 190, 8);
        Assets.images.zombieAttackHatSpriteFactory = g.newSprite("zombie-attacking-hat.png", Graphics.ImageFormat.ARGB8888, 157, 190, 8);

        Assets.images.hurtBloodSpriteFactory = g.newSprite("hurt-blood-sprite.png", Graphics.ImageFormat.ARGB8888, 100, 215, 6);

        Assets.images.hearthExplodeSpriteFactory = g.newSprite("heart-broken-sprite.png", Graphics.ImageFormat.ARGB8888, 72, 62, 7);
        Assets.images.hearthEmpty = g.newImage("heart-empty.png", Graphics.ImageFormat.ARGB8888);
        Assets.images.hearthFull = g.newImage("heart-full.png", Graphics.ImageFormat.ARGB8888);


                Assets.sounds.click = game.getAudio().createSound("click.wav");


       game.setScreen(new MainMenuScreen(game));


    }

    @Override
    public void paint(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawString("Carregando", 640, 350, paint);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void backButton() {

    }
}
