package com.hidra.framework.interfaces;


import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Graphics {


    public static enum ImageFormat {
        ARGB8888, ARGB4444, RGB565
    }

    Image newImage(String fileName, ImageFormat format);

    SpriteFactory newSprite(String fileName, ImageFormat format, int frameWidth, int frameHeight, int frameCount);

    Typeface loadFont(String name);

    void clearScreen(int color);

    void drawLine(int x, int y, int x2, int y2, int color);

    void drawRect(int x, int y, int width, int height, int color);
    void strokeRect(int x, int y, int width, int height, int color);

    void drawArc(int x, int y, int size, int color);
    void drawArc(int x, int y, int size, int startAngle, int endAngle, int color);
    void strokeArc(int x, int y, int size, int startAngle, int endAngle, int color, float width);


    void drawImage(Image image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight);

    void drawImage(Image image, int x, int y);

    void drawImage(Image image, int x, int y, int rotatedeg);

    void drawImage(Image image, int x, int y, int rotatedeg, Point center);


    void drawScaledImage(Image img, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight);
    void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight, int rotateAng);
    void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight, int rotateAng, Point center);


    void drawSprite(Sprite image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight);

    void drawSprite(Sprite image, int x, int y);

    void drawSprite(Sprite image, int x, int y, int rotatedeg);

    void drawScaledSprite(Sprite img, int x, int y, int width, int height, int srcX, int srcY, int srcWidht, int srcHeight);
    
    
    
    void drawString(String text, int x, int y, Paint paint);

    int getWidth();

    int getHeight();

    void drawARGB(int i, int j, int k, int l);

}
