package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Music {

    void play();

    void stop();

    void pause();

    void setLooping(boolean looping);

    void setVolume(float volume);

    boolean isPlaying();

    boolean isStopped();

    void dispose();

    void seekBegin();

}
