package com.castrolol.hidra.archergame.physics;

import android.graphics.Point;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 'Luan on 05/01/2016.
 */
public class CollisionHandler {

    public interface HitboxProvider {
        Hitbox[] getHitboxes();
        void onCollided(int type);
    }

    private static CollisionHandler current;

    public static CollisionHandler getCurrent(){
        if(current == null) current = new CollisionHandler();
        return current;
    }

    Map<String, HitboxProvider> hitboxMap;

    public CollisionHandler(){
        hitboxMap = new HashMap<>();
    }

    public void listen(String id, HitboxProvider hitboxProvider){
        hitboxMap.put(id, hitboxProvider);
    }

    public void remove(String id){
        hitboxMap.remove(id);
    }

    public Point checkCollisions(Point ...points){


        for(HitboxProvider hitboxProvider : hitboxMap.values()){
            for(Hitbox hitbox : hitboxProvider.getHitboxes()){
                for(Point point : points){
                    if(hitbox.contains(point.x, point.y)){
                        hitboxProvider.onCollided(hitbox.type);
                        return point;
                    }
                }
            }
        }
        return  null;
    }


}
