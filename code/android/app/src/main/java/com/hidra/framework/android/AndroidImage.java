package com.hidra.framework.android;

import android.graphics.Bitmap;

import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Graphics.ImageFormat;
import com.hidra.framework.interfaces.Image;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class AndroidImage implements Image {

    Bitmap bitmap;
    Graphics.ImageFormat format;

    public AndroidImage(Bitmap bitmap, ImageFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }


    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    @Override
    public ImageFormat getFormat() {
        return format;
    }

    @Override
    public void dispose() {
        bitmap.recycle();
    }


}
