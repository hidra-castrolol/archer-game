package com.castrolol.hidra.archergame;

import android.graphics.Point;

import com.castrolol.hidra.archergame.objects.Button;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Input;
import com.hidra.framework.interfaces.Screen;

import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class MainMenuScreen extends Screen {

    public MainMenuScreen(final Game game) {
        super(game);
        playButton = new Button(game, new Point(400, 450), new Point(400, 300));

        playButton.setButtonClick(new Button.ButtonClick() {
            @Override
            public void onClick(Input.TouchEvent ev) {
                game.setScreen(new GameScreen(game));
            }
        });

        add(playButton);


    }

    Button playButton;


    @Override
    public void update(float deltaTime) {


    }


    @Override
    public void paint(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawScaledImage(Assets.images.menuBackground, 0, 0, 1300, 800, 0, 0, 1200, 800);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void backButton() {

    }
}
