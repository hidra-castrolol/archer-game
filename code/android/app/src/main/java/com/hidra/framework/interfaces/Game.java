package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Game {

    Audio getAudio();
    Input getInput();
    FileIO getFileIO();
    Graphics getGraphics();
    void setScreen(Screen screen);
    Screen getCurrentScreen();
    Screen getInitScreen();

}
