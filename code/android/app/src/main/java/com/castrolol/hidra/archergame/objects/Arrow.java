package com.castrolol.hidra.archergame.objects;

import android.graphics.Color;
import android.graphics.Point;

import com.castrolol.hidra.archergame.Assets;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 02/01/2016.
 */
public class Arrow implements GameObject {
    private static final float GRAVITY = 9.8f;
    private final Game game;
    private final float angle;
    private final float rad;
    private final float speed;
    private final int x;
    private final int y;
    private float currAngle;
    private float time;

    private int lastX, lastY;
    private int calculedX, calculedY;
    private int nextX, nextY;

    public Arrow(Game game, float angle, float speed, int x, int y) {

        this.game = game;

        this.speed = speed;
        this.angle = angle;
        this.rad = angle;
        this.x = x;
        this.y = y;
        time = 0;

        this.currAngle = angle;

        this.calculedX = 800 - x;
        this.calculedY = y;
        lastX = calculedX;
        lastY = calculedY;

        update(0);

    }

    @Override
    public void paint(float delta) {

        Graphics g = game.getGraphics();
        g.drawImage(Assets.images.arrow, calculedX - 50, calculedY, ((int) Math.toDegrees(currAngle) + 90) % 360);


    }

    @Override
    public void update(float delta) {
        lastX = calculedX;
        lastY = lastX;
        time += (delta / 10);

        calculedY = 800 - (y + calcY());
        calculedX = x + calcX();


        nextX = x + calcX(0.5f);
        nextY = 800 - (y + calcY(0.5f));

        float rad = (float) Math.atan2(calculedX - nextX, calculedY - nextY);
        currAngle = rad;

    }

    private int calcY() {
        return calcY(0);
    }

    private int calcY(float timePlus) {
        float t = time + timePlus;
        int y = (int) ((speed * Math.sin(rad)) * t - ((GRAVITY * .5) * t * t));
        return y;
    }

    private int calcX() {
        return calcX(0);
    }

    private int calcX(float timePlus) {
        float t = time + timePlus;
        return (int) ((speed * Math.cos(rad)) * t);
    }

    @Override
    public void dispose() {

    }

    public int getX() {
        return calculedX;
    }

    public int getY() {
        return calculedY;
    }

    public float getDegrees() {
        return currAngle;
    }

    public Point[] getPositions() {
        return new Point[]{
               // new Point(lastX, lastY),
                new Point(calculedX, calculedY),
                new Point(nextX, nextY),
        };
    }
}
