package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Image {

    int getWidth();
    int getHeight();
    Graphics.ImageFormat getFormat();
    void dispose();

}
