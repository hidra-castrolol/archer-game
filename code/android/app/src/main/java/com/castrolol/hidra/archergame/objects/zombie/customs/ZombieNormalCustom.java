package com.castrolol.hidra.archergame.objects.zombie.customs;

import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 09/01/2016.
 */
public class ZombieNormalCustom extends ZombieBaseCustom {

    public ZombieNormalCustom(){
        animationCompleted = true;
    }

    @Override
    public int getMaximumHp() {
        return 100;
    }

    @Override
    public float getMaximumSpeed() {
        return 0.85f;
    }

    @Override
    public float getRecoverySpeedAmount() {
        return 0;
    }

    @Override
    public void paint(float delta, int x, int y, Graphics g) {

    }

    @Override
    public void update(float delta) {

    }




    @Override
    public void reset() {

    }

    @Override
    public float getMultiplier() {
        return 1;
    }
}
