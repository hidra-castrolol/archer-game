package com.castrolol.hidra.archergame.objects;

import com.hidra.framework.base.Pool;

/**
 * Created by 'Luan on 10/01/2016.
 */
public class Timer {

    private static Pool<Timer> pool;

    public static Timer create(){

        if(pool == null){
            pool = new Pool<>(new Pool.PoolObjectFactory<Timer>() {
                @Override
                public Timer createObject() {
                    return new Timer();
                }
            }, 30);
        }

        return pool.newObject();

    }


    private long startTime;

    private Timer(){

    }

    public void reset(){
        startTime = 0;
    }

    public boolean isElapsed(int time){
        if(startTime == 0){
            startTime = System.nanoTime() / 1000000;
            return false;
        }

        long currentTime = System.nanoTime() / 1000000;

        if(currentTime - startTime >= time){

            return true;
        }
        return false;

    }


    public void dispose(){
        pool.free(this);
    }

    public float getElapsed() {
        if(startTime == 0){
            startTime = System.nanoTime() / 1000000;

        }
        long currentTime = System.nanoTime() / 1000000;

        return currentTime - startTime;
    }
}
