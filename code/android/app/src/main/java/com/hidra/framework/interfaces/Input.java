package com.hidra.framework.interfaces;

import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Input {

    public static class TouchEvent {
        public static final int TOUCH_DOWN = 0;
        public static final int TOUCH_UP = 1;
        public static final int TOUCH_DRAGGED = 2;
        public static final int TOUCH_HOLD = 3;


        public int type;
        public int x, y;
        public int pointer;

    }

    boolean isTouchDown(int pointer);
    int getTouchX(int pointer);
    int getTouchY(int pointer);
    List<TouchEvent> getTouchEvents();

}
