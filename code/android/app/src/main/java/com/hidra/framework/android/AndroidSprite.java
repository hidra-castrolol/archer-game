package com.hidra.framework.android;

import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;

/**
 * Created by 'Luan on 05/01/2016.
 */
public class AndroidSprite extends Sprite {

    private final int frameWidth;
    private final int frameHeight;

    public final Image source;

    public AndroidSprite(Image image, int frameWidth, int frameHeight, int frameCount, float timePerFrame) {
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameCount = frameCount;
        this.timePerFrame = timePerFrame;
        this.timeElapsed = 0;
        this.source = image;
    }


    @Override
    public int getHeight() {
        return frameHeight;
    }

    @Override
    public int getWidth() {
        return frameWidth;
    }

    @Override
    public int getSourceX() {
        return frameIndex * frameWidth;
    }

    @Override
    public int getSourceY() {
        return 0;
    }

    public void dispose() {
        source.dispose();
    }
}
