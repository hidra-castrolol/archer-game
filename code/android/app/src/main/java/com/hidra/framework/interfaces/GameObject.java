package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 30/12/2015.
 */
public interface GameObject {
    void paint(float delta);
    void update(float delta);
    void dispose();
}
