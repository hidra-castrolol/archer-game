package com.castrolol.hidra.archergame.objects;

import android.graphics.Point;
import android.graphics.Rect;

import com.castrolol.hidra.archergame.Assets;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Input;

/**
 * Created by 'Luan on 31/12/2015.
 */
public class Button implements GameObject {

    public interface ButtonClick {
        void onClick(Input.TouchEvent ev);
    }

    private final Game game;
    private final Point position;
    private final Point size;
    private final Rect bounds;
    private boolean isPressed;
    private ButtonClick buttonClick;


    public Button(Game game, Point position, Point size){
        this.game = game;
        this.position = position;
        this.size =size;
        this.bounds = new Rect(this.position.x, this.position.y, this.position.x + this.size.x, this.position.y + this.size.y);
    }

    public void setButtonClick(ButtonClick buttonClick){
        this.buttonClick = buttonClick;
    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();
        Image btn =  Assets.images.playButton;
        if(isPressed) btn = Assets.images.playButtonDown;

        g.drawImage(btn, position.x, position.y, 0, 0, size.x, size.y );
    }

    @Override
    public void update(float delta) {
        Input input = game.getInput();
        if(!isPressed){


            for(Input.TouchEvent touchEvent : input.getTouchEvents()){
                if(touchEvent.type == Input.TouchEvent.TOUCH_DOWN){
                    if(bounds.contains(touchEvent.x, touchEvent.y)){
                        isPressed = true;
                        break;
                    }
                }
            }

        }else {
            for(Input.TouchEvent touchEvent : input.getTouchEvents()){
                if(touchEvent.type == Input.TouchEvent.TOUCH_UP){
                    isPressed = false;
                    if(bounds.contains(touchEvent.x, touchEvent.y)){

                        if(buttonClick != null) buttonClick.onClick((touchEvent));
                        break;
                    }
                }
            }
        }

    }

    @Override
    public void dispose() {

    }
}
