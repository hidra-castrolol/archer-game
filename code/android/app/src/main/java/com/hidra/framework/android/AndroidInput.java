package com.hidra.framework.android;

import android.content.Context;
import android.os.Build;
import android.view.View;

import com.hidra.framework.interfaces.Input;
import com.hidra.framework.interfaces.TouchHandler;

import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class AndroidInput implements Input {
    TouchHandler touchHandler;

    List<TouchEvent> bufferedTouches;

    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        if (Integer.parseInt(Build.VERSION.SDK) < 5) {
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        } else {
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);
        }
    }

    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }

    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }

    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }

    public void clearBuffer(){
        bufferedTouches = null;
    }

    @Override
    public List<TouchEvent> getTouchEvents() {
        if (bufferedTouches == null)
            bufferedTouches = touchHandler.getTouchEvents();
        return bufferedTouches;
    }
}
