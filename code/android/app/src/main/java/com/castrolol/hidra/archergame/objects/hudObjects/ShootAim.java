package com.castrolol.hidra.archergame.objects.hudObjects;

import android.graphics.Point;

import com.castrolol.hidra.archergame.Assets;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Input;

import java.util.List;

/**
 * Created by 'Luan on 25/01/2016.
 */
public class ShootAim implements GameObject {


    public float getForce() {
        return force;
    }

    public float getAngle() {
        return angle;
    }

    public enum PressState {
        Released,
        Pressed
    }
    public static final int MIN_FORCE = 10;
    public static final int MAX_FORCE = 120;
    public static final int MIN_ANGLE = 0;
    public static final int MAX_ANGLE = 60;
    public final Game game ;

    public final Image[] images;

    private PressState state = PressState.Released;

    private float force;

    private int startX;
    private int startY;
    private int x;
    private int y;
    private int endX;
    private int endY;
    private float angle;
    private boolean shootReady;
    private boolean canPrepare;



    public float getRadians() {
        return (float)Math.toRadians(angle);
    }

    public ShootAim(Game game){
        this.game = game;
        images = new Image[]{
                Assets.images.bow1,
                Assets.images.bow2,
                Assets.images.bow3,
                Assets.images.bow4,
                Assets.images.bow5,
                Assets.images.bow6,
                Assets.images.bow7,
                Assets.images.bow8,
                Assets.images.bow9,
                Assets.images.bow10,
        };
    }

    private Image getImage(){

        int f = Math.max((int) force - MIN_FORCE, 0);
        f = Math.round( f / ((MAX_FORCE - MIN_FORCE) / (images.length - 1)) );
        return images[f];

    }

    @Override
    public void paint(float delta) {

        if(force < MIN_FORCE) return;

        Image img = getImage();

        Graphics g = game.getGraphics();

        g.drawScaledImage(img, startX - 130, startY - 159, 134 ,318, 0,0,200,477, (int) angle, new Point(130, 159) );

        float recuoFlecha = (force * 3.5f / 1.5f);

        g.drawScaledImage(Assets.images.arrowBig, (int)((float)startX - recuoFlecha), startY - 10, 300, 20, 0,0,451,29, (int) angle, new Point((int)recuoFlecha, 10 ));


    }

    @Override
    public void update(float delta) {

        Input input = game.getInput();

        if(!shootReady && canPrepare) {
            updateInput(input);
        }

        if(state == PressState.Pressed) {
            updateValues();
        }

    }


    public void setCanPrepare(boolean canPrepare) {
        this.canPrepare = canPrepare;
    }

    public boolean isShootReady(){
        return shootReady;
    }

    public void resetValues() {

        force = 0;
        x = 0;
        y = 0;
        endX = 0;
        endY = 0;
        angle = 0;
        state = PressState.Released;
        shootReady = false;

    }

    private void updateValues() {
        int startY = 800 - this.startY;
        int y = 800 - this.y;

        y = Math.min(startY, y);
        int x = Math.min(startX, this.x);

        float velocity = (float) Math.sqrt(Math.pow(startX - x, 2) + Math.pow(startY - y, 2));
        force = velocity / 3.5f;

        if(force < MIN_FORCE) return;

        float sin = Math.abs(startY - y) / velocity;
        float rad = (float) Math.asin(sin);

        angle = (float) Math.toDegrees(rad);
        angle = Math.abs(angle % 90);
        angle = Math.max(MIN_ANGLE, Math.min(angle, MAX_ANGLE));

        force = Math.max(MIN_FORCE,Math.min(force, MAX_FORCE));
    }

    private void updateInput(Input input) {
        if (state == PressState.Released) {

            List<Input.TouchEvent> touchEventList = input.getTouchEvents();
            for (Input.TouchEvent touchEvent : touchEventList) {
                if (touchEvent.type == Input.TouchEvent.TOUCH_DOWN) {
                    startX = touchEvent.x;
                    startY = touchEvent.y;
                    state = PressState.Pressed;
                }
            }
        } else {

            List<Input.TouchEvent> touchEventList = input.getTouchEvents();

            boolean someDown = false;
            boolean someUp = false;

            for (Input.TouchEvent touchEvent : touchEventList) {
                if (touchEvent.type == Input.TouchEvent.TOUCH_DOWN) {
                    someDown = true;
                } else if (touchEvent.type == Input.TouchEvent.TOUCH_UP) {

                    endX = touchEvent.x;
                    endY = touchEvent.y;
                    someUp = true;
                } else if (touchEvent.type == Input.TouchEvent.TOUCH_DRAGGED) {

                    x = touchEvent.x;
                    y = touchEvent.y;
                }
            }

            // someUp = touchEventList.isEmpty();

            if (!someDown && someUp) {
                state = PressState.Released;
                shootReady = true;
                x = 0;
                y = 0;
            }


        }
    }

    @Override
    public void dispose() {

    }
}
