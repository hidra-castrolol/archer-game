package com.castrolol.hidra.archergame.objects.zombie.types;

import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseType;
import com.castrolol.hidra.archergame.objects.zombie.customs.ZombieHatCustom;
import com.castrolol.hidra.archergame.objects.zombie.customs.ZombieNormalCustom;
import com.hidra.framework.base.Pool;

import java.util.Random;

/**
 * Created by 'Luan on 10/01/2016.
 */
public class ZombieTypeFactory {

    private final Pool<ZombieFatType> poolNormal;

    public ZombieTypeFactory(){


        poolNormal = new Pool<>(new Pool.PoolObjectFactory<ZombieFatType>() {
            @Override
            public ZombieFatType createObject() {
                return new ZombieFatType();
            }
        }, 30);



    }

    private static ZombieTypeFactory current;

    public static ZombieTypeFactory getCurrent(){
        if(current == null){
            current = new ZombieTypeFactory();
        }
        return current;
    }

    public void free(ZombieBaseType zombieType){

        if(zombieType instanceof ZombieFatType){
            poolNormal.free((ZombieFatType)zombieType);
        }

    }

    public ZombieBaseType create(){
        ZombieBaseType type = chooseType();
        type.reset();
        return type;
    }

    private ZombieBaseType chooseType(){
        return poolNormal.newObject();

    }


}
