package com.castrolol.hidra.archergame.objects;


import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.objects.hudObjects.Cooldown;
import com.castrolol.hidra.archergame.objects.hudObjects.ShootAim;
import com.hidra.framework.android.AndroidGame;
import com.hidra.framework.android.AndroidGraphics;
import com.hidra.framework.android.Event;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;
import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Input;
import com.hidra.framework.interfaces.Sprite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Luan on 30/12/2015.
 */
public class Archer implements GameObject {

    private static Archer archer;

    public static Archer getArcher() {
        return archer;
    }

    private double MAX_ANGLE_RAD = Math.toRadians(60);
    private double MIN_ANGLE_RAD = Math.toRadians(0);

    //events

    public final static Event<Archer> onDamage = new Event<>("ondamaged");
    public final static Event<Archer> onCreated = new Event<>("oncreated");


    private Sprite hurtSprite;

    public List<Arrow> getArrows() {
        return arrows;
    }

    public int getLives() {
        return lives;
    }

    public interface OnShootArrowListener {
        void onShoot(Arrow arrow);
    }

    private final Game game;
    private OnShootArrowListener listener;


    private Cooldown cooldown;
    private ShootAim shootAim;
    private int lives;
    List<Arrow> arrows;

    public void takeAHurt() {
        lives--;
        hurtSprite.reset();
        onDamage.emit(this);

    }

    public Archer(Game game) {
        archer = this;
        this.game = game;
        arrows = new ArrayList<>();
        cooldown = new Cooldown(game, 750);
        shootAim = new ShootAim(game);

        hurtSprite = Assets.images.hurtBloodSpriteFactory.newSprite(12);
        hurtSprite.setRunOnce(true);
        hurtSprite.setFrameIndex(5);


        lives = 7;

        onCreated.emit(this);
    }

    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();


        Image img = getArcherImage();

        g.drawScaledImage(img, 0, 500, 210, 260, 0, 0, 210, 260);

        if (!hurtSprite.isFinished()) {
            g.drawSprite(hurtSprite, 50, 550);
        }

        cooldown.paint(delta);
        shootAim.paint(delta);

        for (Arrow arrow : arrows) arrow.paint(delta);
    }

    private Image getArcherImage() {

        int n = (int) (shootAim.getAngle() / 7f);

        switch (n) {
            case 0:
                return Assets.images.archer0;
            case 1:
                return Assets.images.archer7;
            case 2:
                return Assets.images.archer15;
            case 3:
                return Assets.images.archer22;
            case 4:
                return Assets.images.archer30;
            case 5:
                return Assets.images.archer37;
            case 6:
                return Assets.images.archer45;
            case 7:
                return Assets.images.archer52;
            case 8:
                return Assets.images.archer60;
            case 9:
            default:
                return Assets.images.archer67;

        }

    }

    @Override
    public void update(float delta) {
        hurtSprite.update(delta);

        for (Arrow arrow : arrows) arrow.update(delta);

        cooldown.update(delta);
        shootAim.setCanPrepare(cooldown.isElapsed());
        shootAim.update(delta);


        if (shootAim.isShootReady()) {
            shootArrow();
            shootAim.resetValues();
            cooldown.reset();
            cooldown.isElapsed();

        }


    }


    private void shootArrow() {


        float angle = shootAim.getAngle();
        float rad = shootAim.getRadians();
        float force = shootAim.getForce();


        Arrow arrow = new Arrow(game, rad, force, 80, 180 + (int) (angle * .7));
        arrows.add(arrow);


    }

    @Override
    public void dispose() {

    }
}
