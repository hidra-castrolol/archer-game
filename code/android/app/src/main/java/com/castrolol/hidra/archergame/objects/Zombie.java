package com.castrolol.hidra.archergame.objects;

import com.castrolol.hidra.archergame.base.CollisionData;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseCustom;
import com.castrolol.hidra.archergame.interfaces.ZombieBaseType;
import com.castrolol.hidra.archergame.objects.zombie.customs.ZombieCustomFactory;
import com.castrolol.hidra.archergame.objects.zombie.types.ZombieTypeFactory;
import com.castrolol.hidra.archergame.physics.CollisionHandler;
import com.castrolol.hidra.archergame.physics.Hitbox;
import com.hidra.framework.android.Event;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;

import java.util.UUID;

/**
 * Created by 'Luan on 10/01/2016.
 */
public class Zombie implements GameObject, CollisionHandler.HitboxProvider {

    public static final String DEATH_EVENT = "ondeath";
    public static final String DAMAGED_EVENT = "ondamaged";


    public static final Event<Zombie> onDamaged = new Event<>(DAMAGED_EVENT);
    public static final Event<Zombie> onDeath = new Event<>(DEATH_EVENT);

    protected final Game game;

    public ZombieBaseCustom custom;
    public ZombieBaseType type;

    public int x;
    public int y;
    public int w;
    public int h;

    public int hp;
    public float speed;
    public float currentSpeed;
    private String id;
    private boolean dying;
    private Timer deathTimer;
    private boolean dead;
    private boolean damager;

    private boolean attacking = false;

    public Zombie(Game game){
        this.id = UUID.randomUUID().toString();
        this.game = game;
        this.deathTimer = Timer.create();
    }

    public Zombie init(){
        type = ZombieTypeFactory.getCurrent().create();
        custom = ZombieCustomFactory.getCurrent().create(type);

        type.setZombie(this);
        custom.setZombie(this);
        attacking = false;
        deathTimer.reset();
        x = type.getStartX();
        y = type.getStartY();
        w = type.getWidth();
        h = type.getHeight();
        damager = false;

        dead = false;
        dying = false;

        speed = custom.getMaximumSpeed();
        hp = custom.getMaximumHp();
        CollisionHandler.getCurrent().listen(this.id, this);

        return this;
    }


    @Override
    public void paint(float delta) {

        Graphics g = game.getGraphics();

        int xDraw = x - (int) ((float) w * .75f);
        int yDraw = y;

        type.paint(delta, xDraw, yDraw, g);
        custom.paint(delta, xDraw, yDraw, g);
    }

    @Override
    public void update(float delta) {


        if(!isDying()) {
            ensureSpeed(delta);


            if (!attacking) {
                attacking = true;
            } else if(attacking && !damager){
                if(type.isPreparedToAttack()){
                    damager = true;
                    Archer.getArcher().takeAHurt();
                }
            }
        }

        type.update(delta);
        custom.update(delta);



        if(dying && type.isAnimationCompleted() && custom.isAnimationCompleted()){
            if(deathTimer.isElapsed(2000)){
                dead = true;
            }
        }

        if(attacking && type.isAttackingAnimationCompleted()){
            attacking = false;
        }

    }

    private void playAttack() {

    }

    private void ensureSpeed(float delta) {
        if(currentSpeed < speed){
            currentSpeed = Math.min(speed, currentSpeed + (speed * delta / 50f));
        }
    }

    @Override
    public void dispose() {
        deathTimer.dispose();
    }

    @Override
    public Hitbox[] getHitboxes() {
        return type.getHitboxes();
    }

    @Override
    public void onCollided(int collisionType) {
        CollisionData data = type.getCollisionData(collisionType);
        applyDamage(data);
    }



    private void applyDamage(CollisionData data) {

        hp -= data.damage;
        currentSpeed -= (speed * data.slow);

        onDamaged.emit(this, data.id);

        if (hp <= 0) {
            dying = true;
            CollisionHandler.getCurrent().remove(this.id);
            onDeath.emit(this, data.id);
        }

    }

    public boolean isDying(){
        return dying;
    }

    public boolean isDead(){
        return dead;
    }

    public float getPointsFor(int reason) {

        return type.getPointsFor(reason) * custom.getMultiplier();

    }

    public boolean isAttacking() {
        return attacking;
    }
}
