package com.castrolol.hidra.archergame.objects.hudObjects;

import android.graphics.Color;
import android.graphics.Paint;

import com.castrolol.hidra.archergame.Assets;
import com.castrolol.hidra.archergame.objects.Archer;
import com.castrolol.hidra.archergame.objects.Zombie;
import com.hidra.framework.android.Event;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.GameObject;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 25/01/2016.
 */
public class PointsPanel implements GameObject, Event.EventListener<Zombie> {

    private final Game game;
    private int points = 0;
    private int lastPoints = 0;
    private float size = 42;
    private float maxSize = 42;
    private final Paint pointStrokePaint;
    private final Paint pointFillPaint;

    public PointsPanel(Game game){
        this.game = game;

        pointStrokePaint = new Paint();
        pointFillPaint = new Paint();

        createPaints();

        setUpEvents();
    }

    private void setUpEvents() {

        Zombie.onDamaged.listen(this);
        Zombie.onDeath.listen(this);

    }

    private void createPaints() {
        pointStrokePaint.setAntiAlias(true);
        pointStrokePaint.setTypeface(Assets.fonts.nightBird);
        pointStrokePaint.setTextSize(42);
        pointStrokePaint.setStyle(Paint.Style.STROKE);
        pointStrokePaint.setTextAlign(Paint.Align.LEFT);
        pointStrokePaint.setColor(Color.rgb(183, 0, 0));

        pointFillPaint.setAntiAlias(true);
        pointFillPaint.setTypeface(Assets.fonts.nightBird);
        pointFillPaint.setTextSize(42);
        pointFillPaint.setStyle(Paint.Style.FILL);
        pointFillPaint.setTextAlign(Paint.Align.LEFT);
        pointFillPaint.setColor(Color.rgb(255, 61, 61));
        pointFillPaint. setStrokeJoin(Paint.Join.BEVEL);
    }


    @Override
    public void paint(float delta) {
        Graphics g = game.getGraphics();

        g.drawImage(Assets.images.blood, 30, 30);

        int baseY = 65;

        if(size > maxSize){
            baseY += (size - maxSize) / 3;
        }

        g.drawString(points + "", 65, baseY, pointFillPaint);
        g.drawString(points + "", 65 , baseY, pointStrokePaint);

    }

    @Override
    public void update(float delta) {

        if(lastPoints != points){

            if(size > maxSize){
                size -= (maxSize * .025 * delta);
            }else{
                lastPoints = points;
                size = maxSize;
            }
            pointStrokePaint.setTextSize(size);
            pointFillPaint.setTextSize(size);
        }

    }

    @Override
    public void dispose() {
        Zombie.onDamaged.remove(this);
        Zombie.onDeath.remove(this);
    }

    @Override
    public void handle(String eventName, Zombie source, Object... params) {

        float mult = 0.5f;
        int reason = (int)params[0];
        float basePoints = source.getPointsFor(reason);

        if(eventName == Zombie.DEATH_EVENT){
            mult = 1f;
        }

        points += basePoints * mult;
        size =  maxSize * 1.75f;

        pointStrokePaint.setTextSize(size);
        pointFillPaint.setTextSize(size);
    }
}
