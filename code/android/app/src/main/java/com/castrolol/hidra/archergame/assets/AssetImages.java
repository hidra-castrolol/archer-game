package com.castrolol.hidra.archergame.assets;

import com.hidra.framework.interfaces.Image;
import com.hidra.framework.interfaces.Sprite;
import com.hidra.framework.interfaces.SpriteFactory;

/**
 * Created by 'Luan on 31/12/2015.
 */
public class AssetImages {
    public Image gameBackground;
    public Image menuBackground;
    public Image playButton;
    public Image playButtonDown;

    public Image shootIndicatorArrow;
    public Image shootIndicatorBoard;

    public Image archer0;
    public Image archer7;
    public Image archer15;
    public Image archer22;
    public Image archer30;
    public Image archer37;
    public Image archer45;
    public Image archer52;
    public Image archer60;
    public Image archer67;

    public Image bow1;
    public Image bow2;
    public Image bow3;
    public Image bow4;
    public Image bow5;
    public Image bow6;
    public Image bow7;
    public Image bow8;
    public Image bow9;
    public Image bow10;


    public Image hearthFull;
    public Image hearthEmpty;

    public Image blood;

    public SpriteFactory zombieWalkSpriteFactory;
    public SpriteFactory zombieHatSpriteFactory;

    public SpriteFactory zombieHeadshotSpriteFactory;
    public SpriteFactory zombieDeathSpriteFactory;
    public SpriteFactory zombieDeathHatSpriteFactory;
    public SpriteFactory zombieAttackSpriteFactory;
    public SpriteFactory zombieAttackHatSpriteFactory;

    public SpriteFactory hurtBloodSpriteFactory;
    public SpriteFactory hearthExplodeSpriteFactory;

    public SpriteFactory fatZombieWalkSpriteFactory;

    public Image arrow;

    public SpriteFactory bloodSpriteFactory;
    public Image arrowBig;
}