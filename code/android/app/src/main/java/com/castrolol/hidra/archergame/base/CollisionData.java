package com.castrolol.hidra.archergame.base;

/**
 * Created by 'Luan on 10/01/2016.
 */
public class CollisionData {
    public static final CollisionData empty = new CollisionData(-1, 0, 0);

    public final int id;
    public final float slow;
    public final int damage;

    public CollisionData(int id, int damage, float speed){
        this.id = id;
        this.slow = speed;
        this.damage = damage;
    }



}
