package com.hidra.framework.android;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by 'Luan on 25/01/2016.
 */
public class Event<T>  {

    public interface EventListener<T> {
        void handle(String eventName, T source, Object... params);
    }

    private final String name;

    public Event(String name){
        this.name = name;
    }

    protected List<EventListener> listeners = new ArrayList<>();

    public void listen(EventListener<T> listener){
        listeners.add(listener);
    }

    public void remove(EventListener<T> listener){
        listeners.remove(listener);
    }

    public void emit(T source, Object... params){
        EventListener[] copyOfListeners = new EventListener[listeners.size()];
        copyOfListeners = listeners.toArray(copyOfListeners);
        for(EventListener listener : copyOfListeners) listener.handle(name, source, params);
    }

}
