package com.hidra.framework.interfaces;

import android.view.View;

import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface TouchHandler extends View.OnTouchListener {
    boolean isTouchDown(int pointer);
    int getTouchX(int pointer);
    int getTouchY(int pointer);
    List<Input.TouchEvent> getTouchEvents();
}
