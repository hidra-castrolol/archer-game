package com.hidra.framework.android;

import android.text.method.Touch;
import android.view.MotionEvent;
import android.view.View;

import com.hidra.framework.base.Pool;
import com.hidra.framework.interfaces.Input;
import com.hidra.framework.interfaces.TouchHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 'Luan on 29/12/2015.
 */
public class MultiTouchHandler implements TouchHandler {

    private static final int MAX_TOUCHPOINTS = 10;

    boolean[] isTouched = new boolean[MAX_TOUCHPOINTS];
    int[] touchX = new int[MAX_TOUCHPOINTS];
    int[] touchY = new int[MAX_TOUCHPOINTS];
    int[] id = new int[MAX_TOUCHPOINTS];
    Pool<Input.TouchEvent> touchEventPool;
    List<Input.TouchEvent> touchEvents = new ArrayList<>();
    List<Input.TouchEvent> touchEventsBuffer = new ArrayList<>();
    float scaleX;
    float scaleY;

    public MultiTouchHandler(View view, float scaleX, float scaleY) {
        Pool.PoolObjectFactory<Input.TouchEvent> factory = new Pool.PoolObjectFactory<Input.TouchEvent>() {
            @Override
            public Input.TouchEvent createObject() {
                return new Input.TouchEvent();
            }
        };

        touchEventPool = new Pool<>(factory, 100);
        view.setOnTouchListener(this);

        this.scaleX = scaleX;
        this.scaleY = scaleY;

    }


    @Override
    public boolean isTouchDown(int pointer) {
        synchronized (this){
            int index = getIndex(pointer);
            if(index < 0 || index >= MAX_TOUCHPOINTS){
                return false;
            }else{
                return isTouched[index];
            }
        }
    }

    @Override
    public int getTouchX(int pointer) {
        synchronized (this){
            int index = getIndex(pointer);
            if(index < 0 || index >= MAX_TOUCHPOINTS){
                return 0;
            }else{
                return touchX[index];
            }
        }
    }

    @Override
    public int getTouchY(int pointer) {
        synchronized (this){
            int index = getIndex(pointer);
            if(index < 0 || index >= MAX_TOUCHPOINTS){
                return 0;
            }else{
                return touchY[index];
            }
        }
    }

    @Override
    public List<Input.TouchEvent> getTouchEvents() {
        synchronized (this){
            int len = touchEvents.size();
            for(int i = 0; i < len; i++){
                touchEventPool.free(touchEvents.get(i));
            }
            touchEvents.clear();
            touchEvents.addAll(touchEventsBuffer);
            touchEventsBuffer.clear();
            return touchEvents;
        }
    }

    public int getIndex(int pointerId){
        for(int i = 0; i < MAX_TOUCHPOINTS; i++){
            if(id[i] == pointerId){
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        synchronized (this){
            int action = motionEvent.getAction() & motionEvent.ACTION_MASK;
            int pointerIndex = (motionEvent.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
            int pointerCount = motionEvent.getPointerCount();
            Input.TouchEvent touchEvent;
            for(int i = 0; i < MAX_TOUCHPOINTS; i++){
                if(i >= pointerCount){
                    isTouched[i] = false;
                    id[i] = -1;
                    continue;
                }

                int pointerId = motionEvent.getPointerId(i);
                if(motionEvent.getAction() != MotionEvent.ACTION_MOVE && i != pointerIndex){
                    continue;
                }

                switch (action){
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_POINTER_DOWN:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = Input.TouchEvent.TOUCH_DOWN;
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int)(motionEvent.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int)(motionEvent.getY(i) * scaleY);
                        isTouched[i] = true;
                        id[i] = pointerId;
                        touchEventsBuffer.add(touchEvent);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                    case MotionEvent.ACTION_CANCEL:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = Input.TouchEvent.TOUCH_UP;
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int)(motionEvent.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int)(motionEvent.getY(i) * scaleY);
                        isTouched[i] = false;
                        id[i] = -1;
                        touchEventsBuffer.add(touchEvent);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        touchEvent = touchEventPool.newObject();
                        touchEvent.type = Input.TouchEvent.TOUCH_DRAGGED;
                        touchEvent.pointer = pointerId;
                        touchEvent.x = touchX[i] = (int)(motionEvent.getX(i) * scaleX);
                        touchEvent.y = touchY[i] = (int)(motionEvent.getY(i) * scaleY);
                        isTouched[i] = true;
                        id[i] = pointerId;
                        touchEventsBuffer.add(touchEvent);
                        break;
                }

            }
            return true;
        }

    }
}
