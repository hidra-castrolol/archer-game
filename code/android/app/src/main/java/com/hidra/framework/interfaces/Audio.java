package com.hidra.framework.interfaces;

/**
 * Created by 'Luan on 29/12/2015.
 */
public interface Audio {

    Music createMusic(String file);

    Sound createSound(String file);


}
