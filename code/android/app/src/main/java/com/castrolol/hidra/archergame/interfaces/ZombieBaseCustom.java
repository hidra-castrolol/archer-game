package com.castrolol.hidra.archergame.interfaces;

import com.castrolol.hidra.archergame.objects.Zombie;
import com.hidra.framework.interfaces.Game;
import com.hidra.framework.interfaces.Graphics;

/**
 * Created by 'Luan on 09/01/2016.
 */
public abstract class ZombieBaseCustom {

    protected Game game;
    protected Zombie zombie;
    protected boolean animationCompleted = false;

    public abstract int getMaximumHp();
    public abstract float getMaximumSpeed();
    public abstract float getRecoverySpeedAmount();

    public abstract void paint(float delta, int x, int y, Graphics g);
    public abstract void update(float delta);



    public Game getGame(){
        return game;
    }

    public void setGame(Game game){
        this.game = game;
    }

    public Zombie getZombie(){
        return zombie;
    }

    public void setZombie(Zombie zombie){
        this.zombie = zombie;
    }

    public abstract void reset();

    public boolean isAnimationCompleted() {
        return animationCompleted;
    }

    public abstract float getMultiplier() ;
}
